import datetime

db.define_table('barbers',
                Field('barber_name')
                )

db.define_table('timeslots',
                Field('timeslot')
                )

db.define_table('appointments',
                Field('barber_id', 'reference barbers'),
                Field('appointment_date', default=datetime.datetime.now()),
                Field('timeslot_id', 'reference timeslots'),
                Field('user_id', 'reference auth_user')
                )


# need barber, time_slots, users, and appt table
